#include "Background.h"
#include "GameManager.h"

namespace POOL {
namespace BACKGROUND {

	using namespace HOYO;
	using namespace BORDE;

	const int margenH = 58;
	const int margenV = 53;

	Texture2D backgroundTexture;

	static void initHoyos();
	static void initBordes();

	void init() {

		backgroundTexture = LoadTexture("res/MesaPool.png");
		backgroundTexture.width = GAME_MANAGER::screenWidth;
		backgroundTexture.height = GAME_MANAGER::screenHeight;
		initHoyos();
		initBordes();
	}

	void draw() {

		DrawTexture(backgroundTexture, 0, 0, WHITE);

#if DEBUG
		for (int i = 0; i < maxHoyos; i++)
		{
			hoyo[i]->dibujar();
		}

		for (int i = 0; i < maxBordes; i++)
		{
			borde[i]->dibujar();
			for (int j = 0; j < BORDE::maxEsquinas; j++)
			{
				borde[i]->esq[j]->dibujar();
			}
		}
#endif
	}

	const short xOffsetCentro = 2;
	void initHoyos()
	{
		hoyo[static_cast<int>(HoyoUbicacion::ARRIBAIZQ)] = new HOYO::Hoyo({ margenH, margenV });
		hoyo[static_cast<int>(HoyoUbicacion::ARRIBACEN)] = new HOYO::Hoyo({ GAME_MANAGER::screenWidth / 2 - xOffsetCentro, margenV });
		hoyo[static_cast<int>(HoyoUbicacion::ARRIBADER)] = new HOYO::Hoyo({ GAME_MANAGER::screenWidth - margenH, margenV });
		hoyo[static_cast<int>(HoyoUbicacion::ABAJOIZQ)] = new HOYO::Hoyo({ margenH, GAME_MANAGER::screenHeight - margenV });
		hoyo[static_cast<int>(HoyoUbicacion::ABAJOCEN)] = new HOYO::Hoyo({ GAME_MANAGER::screenWidth / 2 - xOffsetCentro, GAME_MANAGER::screenHeight - margenV });
		hoyo[static_cast<int>(HoyoUbicacion::ABAJODER)] = new HOYO::Hoyo({ GAME_MANAGER::screenWidth - margenH, GAME_MANAGER::screenHeight - margenV });
	}

	void initBordes() {

		/*borde[0].tipo = BordeUbicacion::ARRIBA;
		borde[0].rec.width = 455;
		borde[0].rec.height = 62;
		borde[0].rec.x = 120;
		borde[0].rec.y = 0;*/

		borde[0] = new BORDE::Borde(BORDE::BordeUbicacion::ARRIBA, { 120, 0, 455, 62 });

		/*borde[1].tipo = BordeUbicacion::ARRIBA;
		borde[1].rec.width = 460;
		borde[1].rec.height = 62;
		borde[1].rec.x = 700;
		borde[1].rec.y = 0;*/

		borde[1] = new BORDE::Borde(BORDE::BordeUbicacion::ARRIBA, { 700, 0, 460, 62 });

		/*borde[2].tipo = BordeUbicacion::DERECHA;
		borde[2].rec.width = 60;
		borde[2].rec.height = 490;
		borde[2].rec.x = GAME_MANAGER::screenWidth - borde[2].rec.width;
		borde[2].rec.y = 115;*/

		borde[2] = new BORDE::Borde(BORDE::BordeUbicacion::DERECHA, { GAME_MANAGER::screenWidth - 60, 115, 60, 490 });

		/*borde[3].tipo = BordeUbicacion::ABAJO;
		borde[3].rec.width = borde[0].rec.width;
		borde[3].rec.height = 62;
		borde[3].rec.x = borde[1].rec.x;
		borde[3].rec.y = GAME_MANAGER::screenHeight - borde[3].rec.height;*/

		borde[3] = new BORDE::Borde(BORDE::BordeUbicacion::ABAJO, { borde[1]->getRec().x, GAME_MANAGER::screenHeight - 62, borde[0]->getRec().width, 62 });

		/*borde[4].tipo = BordeUbicacion::ABAJO;
		borde[4].rec.width = borde[0].rec.width;
		borde[4].rec.height = 62;
		borde[4].rec.x = borde[0].rec.x;
		borde[4].rec.y = GAME_MANAGER::screenHeight - borde[4].rec.height;*/

		borde[4] = new BORDE::Borde(BORDE::BordeUbicacion::ABAJO, { borde[0]->getRec().x, GAME_MANAGER::screenHeight - 62, borde[0]->getRec().width, 62 });

		/*borde[5].tipo = BordeUbicacion::IZQUIERDA;
		borde[5].rec.width = 60;
		borde[5].rec.height = borde[2].rec.height;
		borde[5].rec.x = 0;
		borde[5].rec.y = borde[2].rec.y;*/

		borde[5] = new BORDE::Borde(BORDE::BordeUbicacion::IZQUIERDA, { 0, borde[2]->getRec().y, 60, borde[2]->getRec().height });

		for (int i = 0; i < maxBordes; i++)
		{
			for (int j = 0; j < BORDE::maxEsquinas; j++)
			{
				if (borde[i]->getRec().width > borde[i]->getRec().height)
				{
					borde[i]->esq[j]->setRadio(borde[i]->getRec().height / 2);
					borde[i]->esq[j]->setCir({ borde[i]->getRec().x + (j * (borde[i]->getRec().width)), borde[i]->getRec().y + borde[i]->getRec().height / 2 });
				}
				else 
				{
					borde[i]->esq[j]->setRadio(borde[i]->getRec().width / 2);
					borde[i]->esq[j]->setCir({ borde[i]->getRec().x + borde[i]->getRec().width / 2, borde[i]->getRec().y + (j * borde[i]->getRec().height) });
				}
			}
		}
	}
}
}