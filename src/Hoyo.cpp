#include "Hoyo.h"

namespace POOL {
	namespace HOYO {

		HOYO::Hoyo* hoyo[maxHoyos];

		void deInit()
		{
			for (int i = 0; i < maxHoyos; i++)
			{
				if (hoyo[i] != NULL)
				{
					delete hoyo[i];
					hoyo[i] = NULL;
				}
			}
		}

		Hoyo::Hoyo()
		{
			this->pos = Vector2();
			this->color = Color();
		}

		Hoyo::Hoyo(Vector2 pos)
		{
			this->pos = pos;
			this->color = GREEN;
		}

		Hoyo::~Hoyo()
		{
		}

		void Hoyo::dibujar()
		{
			DrawCircleLines(pos.x, pos.y, radioHoyos, color);
		}

		Vector2 Hoyo::getPos()
		{
			return this->pos;
		}

		void Hoyo::setPos(Vector2 pos)
		{
			this->pos = pos;
		}

		Color Hoyo::getColor()
		{
			return this->color;
		}

		void Hoyo::setColor(Color color)
		{
			this->color = color;
		}
	}
}