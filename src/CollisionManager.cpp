#include "CollisionManager.h"

#include <vector>

#include "Pelota.h"
#include "Palo.h"
#include "Background.h"
#include "Gameplay.h"

namespace POOL {
namespace COLLISION_MANAGER {
using namespace PELOTA;
using namespace PALO;
using namespace BACKGROUND;
using namespace GAMEPLAY;
using namespace ESQUINA;

		static void colisionPelotaPelota();
		static void colisioPelotaEsquina();
		static void colisionPelotaBorde();
		static void colisionPelotaHoyo();

		static float distanciaEntrePuntos(Vector2 p1, Vector2 p2);
		static bool chequearColisionCirCir(float distancia, int p1Radio, int p2Radio);
		static bool chequearColisionBorde(Vector2 posicionPel, float radio, Rectangle rec);

		const float mOffset = 2.0f;

		void collisionManager() {

			colisionPelotaPelota();
			colisioPelotaEsquina();
			colisionPelotaBorde();
			colisionPelotaHoyo();
		}

		const float dezplazamientoMod = 0.5f;
		void colisionPelotaPelota() {
			std::vector < std::pair < Pelota*, Pelota* >> listPelPel;

			float desplazamiento = 0;
			float distancia = 0;
			Vector2 auxCir;

			for (int i = 0; i < maxPelotas; i++) //Por todas las pelotas que hay
			{
				if (pelota[i]->getActivo()) //Siempre que esa pelota este activa
				{
					for (int j = 0; j < maxPelotas; j++) //Me fijo colision con todas las otras pelotas
					{
						if (pelota[j]->getActivo() && j > i) //Except�o el doble chequeo con las anteriores y de la pelota consigo misma
						{
							distancia = distanciaEntrePuntos(pelota[i]->getCir(), pelota[j]->getCir()); //Tomo la distancia entre las pelotas de los indices

							if (chequearColisionCirCir(distancia, pelota[i]->getRadio(), pelota[j]->getRadio()))
							{ //Si la distancia es menor a los radios sumados, entra a este if
								PlaySoundMulti(pelotaPelotaColSound);
								//PlaySound(pelotaPelotaColSound);
								std::cout << "ColisionPP: Pelota: " << i << " - Pelota: " << j << std::endl;

								listPelPel.push_back({ pelota[i], pelota[j] }); //Agrega la colision entre esas 2 pelotas a la lista de colisiones

								desplazamiento = dezplazamientoMod * (distancia - pelota[i]->getRadio() - pelota[j]->getRadio());

								auxCir.x = pelota[i]->getCir().x - desplazamiento * (pelota[i]->getCir().x - pelota[j]->getCir().x) / distancia;
								auxCir.y = pelota[i]->getCir().y - desplazamiento * (pelota[i]->getCir().y - pelota[j]->getCir().y) / distancia;
								pelota[i]->setCir(auxCir);

								auxCir.x = pelota[j]->getCir().x + desplazamiento * (pelota[i]->getCir().x - pelota[j]->getCir().x) / distancia;
								auxCir.y = pelota[j]->getCir().y + desplazamiento * (pelota[i]->getCir().y - pelota[j]->getCir().y) / distancia;
								pelota[j]->setCir(auxCir);

								//Elimina el overlap entre ambas pelotas, en el caso de que se pasen. (IMG 1)

							}
						}
					}
				}
			}

			for (auto c : listPelPel)
			{
				Pelota* p1 = c.first;
				Pelota* p2 = c.second;

				distancia = distanciaEntrePuntos(p1->getCir(), p2->getCir());

				// Es el eje que une los centros (normal)
				Vector2 normal = { (p2->getCir().x - p1->getCir().x) / distancia ,(p2->getCir().y - p1->getCir().y) / distancia }; // Normal o Componente Centr�peta

				// Es el eje perpendicular al eje Normal
				Vector2 tangencial = { -normal.y, normal.x }; // Vector Tangencial X 

				// La tangencial cambia el modulo de la velocidad siempre en numeros positivos
				float vectorTgP1 = p1->getVel().x * tangencial.x + p1->getVel().y * tangencial.y;
				float vectorTgP2 = p2->getVel().x * tangencial.x + p2->getVel().y * tangencial.y;

				// La normal cambia la direccion, osea el signo de la velocidad. Si es que va a ser positiva o negativa.
				float vectorNormP1 = p1->getVel().x * normal.x + p1->getVel().y * normal.y;
				float vectorNormP2 = p2->getVel().x * normal.x + p2->getVel().y * normal.y;

				// Calculo las fuerzas que se aplican al chocar a cada una de las pelotas (Formula de choque el�stico) (FUERZA = MASA * ACELERACION)
				//mOffset si no esta, las pelotas casi no se mueven.
				float fuerzaP1 = (vectorNormP1 * (p1->getMasa() - p2->getMasa()) + mOffset * p2->getMasa() * vectorNormP2) / (p1->getMasa() + p2->getMasa());
				float fuerzaP2 = (vectorNormP2 * (p2->getMasa() - p1->getMasa()) + mOffset * p1->getMasa() * vectorNormP1) / (p1->getMasa() + p2->getMasa());

				// Se aplican las fuerzas obtenidas a ambas pelotas.
				p1->setVel({ tangencial.x * vectorTgP1 + normal.x * fuerzaP1, tangencial.y * vectorTgP1 + normal.y * fuerzaP1 });
				p2->setVel({ tangencial.x * vectorTgP2 + normal.x * fuerzaP2, tangencial.y * vectorTgP2 + normal.y * fuerzaP2 });

				//Se indica que ambas pelotas ent�n en movimiento.
				p1->setEnMovimiento(true);
				p2->setEnMovimiento(true);
			}
		}
		
		void colisioPelotaEsquina() {

			std::vector < std::pair < Pelota*, Esquina* >> listPelEsq;

			float desplazamiento = 0;
			float distancia = 0;
			Vector2 auxCir;

			for (int i = 0; i < maxPelotas; i++) //para cada una de las pelotas
			{
				if (pelota[i]->getActivo()) //Si esa pelota esta activa
				{
					for (int j = 0; j < BORDE::maxBordes; j++) //Para cada uno de los bordes
					{
						for (int k = 0; k < BORDE::maxEsquinas; k++) //Para cada esquina de cada borde
						{
							distancia = distanciaEntrePuntos(pelota[i]->getCir(), BORDE::borde[j]->esq[k]->getCir()); 
							//Tomo la distancia entre el la pelota y la esquina

							if (chequearColisionCirCir(distancia, pelota[i]->getRadio(), BORDE::borde[j]->esq[k]->getRadio()))
							{ //Si la distancia es menor que la suma de los centros de la pelota y la esquina, entra
								PlaySoundMulti(pelotaBordeColSound);
								std::cout << "ColisionPP: Pelota: " << i << " - Esquina: " << j << std::endl;

								listPelEsq.push_back({ pelota[i], BORDE::borde[j]->esq[k] }); //Agrega la colision a la lista de colisiones

								desplazamiento = (distancia - pelota[i]->getRadio() - BORDE::borde[j]->esq[k]->getRadio());

								auxCir.x = pelota[i]->getCir().x - desplazamiento * (pelota[i]->getCir().x - BORDE::borde[j]->esq[k]->getCir().x) / distancia;
								auxCir.y = pelota[i]->getCir().y - desplazamiento * (pelota[i]->getCir().y - BORDE::borde[j]->esq[k]->getCir().y) / distancia;
								pelota[i]->setCir(auxCir);

								//Corrije el overlap porque solo se mueve la pelota, no la esquina

							}
						}
					}
				}
			}

			for (auto c : listPelEsq)
			{
				Pelota* p1 = c.first;
				Esquina* p2 = c.second;

				
				distancia = distanciaEntrePuntos(p1->getCir(), p2->getCir());

				Vector2 normal = { (p2->getCir().x - p1->getCir().x) / distancia ,(p2->getCir().y - p1->getCir().y) / distancia };

				Vector2 tangencial = { -normal.y, normal.x };

				float vectorTgP1 = p1->getVel().x * tangencial.x + p1->getVel().y * tangencial.y;
				float vectorTgP2 = 0;

				float vectorNormP1 = p1->getVel().x * normal.x + p1->getVel().y * normal.y;
				float vectorNormP2 = 0;

				float fuerzaP1 = (vectorNormP1 * (p1->getMasa() - p2->getMasa()) + mOffset * p2->getMasa() * vectorNormP2) / (p1->getMasa() + p2->getMasa());
				float fuerzaP2 = (vectorNormP2 * (p2->getMasa() - p1->getMasa()) + mOffset * p1->getMasa() * vectorNormP1) / (p1->getMasa() + p2->getMasa());

				p1->setVel({ tangencial.x * vectorTgP1 + normal.x * fuerzaP1, tangencial.y * vectorTgP1 + normal.y * fuerzaP1 });

				//Aplica solamente la fuerza a la pelota. LA esquina no va a moverse

				p1->setEnMovimiento(true);
			}
		}
		
		void colisionPelotaBorde() {

			for (int i = 0; i < maxPelotas; i++) 
			{
				if (pelota[i]->getActivo())
				{
					for (int j = 0; j < BORDE::maxBordes; j++)
					{
						if (chequearColisionBorde(pelota[i]->getCir(), pelota[i]->getRadio(), BORDE::borde[j]->getRec()))
						{
							PlaySoundMulti(pelotaBordeColSound);
							std::cout << "ColisionPB: Pelota: " << i << " - borde: " << j << std::endl;
							switch (BORDE::borde[j]->getTipo())
							{
							case BORDE::BordeUbicacion::ARRIBA:
								pelota[i]->setVel({ pelota[i]->getVel().x, fabsf(pelota[i]->getVel().y) });
								break;
							case BORDE::BordeUbicacion::ABAJO:
								pelota[i]->setVel({ pelota[i]->getVel().x, -fabsf(pelota[i]->getVel().y) });
								break;
							case BORDE::BordeUbicacion::DERECHA:
								pelota[i]->setVel({ -fabsf(pelota[i]->getVel().x), pelota[i]->getVel().y });
								break;
							case BORDE::BordeUbicacion::IZQUIERDA:
								pelota[i]->setVel({ fabsf(pelota[i]->getVel().x), pelota[i]->getVel().y });
								break;
							default:
								std::cout << "Borde sin tipo" << std::endl;
								system("pause");
								break;
							}
						}
					}
				}
			}
		}
		
		void colisionPelotaHoyo() {

			for (int i = 0; i < maxPelotas; i++)
			{
				if (pelota[i]->getActivo())
				{
					for (int j = 0; j < HOYO::maxHoyos; j++)
					{
						if (chequeoPuntoCirculo(pelota[i]->getCir(), HOYO::hoyo[j]->getPos(), HOYO::radioHoyos))
						{
							pelota[i]->setDesaparicion(true);
						}
					}
				}
			}
		}

		float distanciaEntrePuntos(Vector2 p1, Vector2 p2)
		{
			return sqrtf(powf((p1.x - p2.x), 2) + powf((p1.y - p2.y), 2)); // Raiz cuadrada de la suma de las componentes de los centros de las pelotas al cuadrado
		}

		bool chequearColisionCirCir(float distancia, int p1Radio, int p2Radio)
		{
			if (distancia < p1Radio + p2Radio) //Suma ambos radios y se fija si hay separacion entre pelotas. 
			{
				return true;
			}
			return false;
		}

		bool chequeoPuntoCirculo(Vector2 point, Vector2 center, int radio)
		{
			float distancia = distanciaEntrePuntos(point, center);

			if (distancia <= radio) 
			{
				return true;
			}
			return false;
		}

		bool chequearColisionBorde(Vector2 posicionPel, float radio, Rectangle rec)
		{
			Vector2 p = posicionPel; 

			//Verifica en que parte del perimetro del cuadrado esta el punto. 

			if (p.x < rec.x) 
			{
				p.x = rec.x;
			}
			else if (p.x > rec.x + rec.width)
			{
				p.x = rec.x + rec.width;
			}

			if (p.y < rec.y)
			{
				p.y = rec.y;
			}
			else if (p.y > rec.y + rec.height)
			{
				p.y = rec.y + rec.height;
			}

			//Chequea que la pelota este dentro del area de juego

			float distancia = distanciaEntrePuntos(posicionPel, p); //distancia entre la nueva posicion y la posicion dada.
			if (distancia < radio)
			{
				return true;
			}
			return false;
		}
	}
}

