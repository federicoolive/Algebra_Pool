#include "raylib.h"

namespace POOL {
	namespace HOYO {

		const int radioHoyos = 36;
		const int maxHoyos = 6;

		void deInit();

		class Hoyo
		{
		public:
			Hoyo();
			Hoyo(Vector2 pos);
			~Hoyo();

			void dibujar();

			Vector2 getPos();
			void setPos(Vector2 pos);
			Color getColor();
			void setColor(Color color);

		private:
			Vector2 pos;
			Color color;
		};

		extern HOYO::Hoyo* hoyo[maxHoyos];
	}
}